package com.example.authserver.service;

import com.example.authserver.entity.UserDao;
import com.example.authserver.repository.UserRepository;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class SpringDataUserDetailsService implements UserDetailsService {
  private final UserRepository userRepository;
  private final BCryptPasswordEncoder passwordEncoder;

  public SpringDataUserDetailsService(
      UserRepository userRepository,
      BCryptPasswordEncoder passwordEncoder) {
    this.userRepository = userRepository;
    this.passwordEncoder = passwordEncoder;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    UserDao userDao = userRepository.findByUsername(username);
    if (userDao == null) {
      throw new UsernameNotFoundException("username:" + username + " not found");
    }
    return User.withUsername(userDao.getUsername())
        .password(passwordEncoder.encode(userDao.getPassword()))
        .authorities("ROLE_ADMIN")
        .build();
  }
}
