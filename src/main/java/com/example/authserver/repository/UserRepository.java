package com.example.authserver.repository;

import com.example.authserver.entity.UserDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserDao, Long> {
  UserDao findByUsername(String username);
}
