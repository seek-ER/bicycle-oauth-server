FROM openjdk:11-jdk-slim

# 配置参数
ARG JAR_FILE=target/*.jar
# 将编译构建得到的jar文件复制到镜像空间中
COPY ${JAR_FILE} application.jar

ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar","application.jar"]

EXPOSE 8091